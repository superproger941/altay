@extends('layouts.default')

@section('title', 'Контакты | ' . config('app.site_title') . ' | '. config('app.name'))

@section('description', 'Контакты. Форма для бронирования аренды/проката авто в Республике Алтай. '. config('app.name'))

@section('content')
    <div class=" bd-content-7">
        <div class=" bd-blog-5 bd-page-width  " itemscope="" itemtype="http://schema.org/Article">
            <div class="bd-container-inner">
                <div class=" bd-grid-7 bd-margins">
                    <div class="container-fluid">
                        <div class="separated-grid row">
                            <div class="separated-item-46 col-md-12 first-col last-row last-col"
                                 style="overflow: visible; height: auto;">
                                <div class="bd-griditem-46">
                                    <article class=" bd-article-4" style="position: relative;">
                                        <div class=" bd-pagetitle-3 bd-background-width ">
                                            <div class="bd-container-inner">
                                                <h1> Контакты </h1>
                                            </div>
                                        </div>
                                        <div class=" bd-postcontent-4 bd-tagstyles bd-contentlayout-offset"
                                             itemprop="articleBody">
                                            <p><!--[html]-->
                                                <style></style>
                                            </p>
                                            <div class="bd-tagstyles  additional-class-1877650297 ">
                                                <div class="bd-container-inner bd-content-element"><!--{content}-->
                                                    <p></p>
                                                    <p></p>
                                                    <div class="qf3form simple">
                                                        <form action="/" method="post"
                                                              enctype="multipart/form-data" autocomplete="off">
                                                            {{ csrf_field() }}
                                                            <div class="qf3 qf3txt qftext req">
                                                                <label class="qf3label">
                                                                    Введите имя
                                                                    <span class="qf3labelreq">*</span>
                                                                </label>
                                                                <input type="text" name="name" required=""></div>
                                                            <div class="qf3 qf3txt qftel req">
                                                                <label class="qf3label">
                                                                    Номер телефона <span class="qf3labelreq">*</span>
                                                                </label>
                                                                <input type="tel" name="phone" required="">
                                                            </div>
                                                            <div class="qf3 qf3txt qfemail">
                                                                <label class="qf3label">Электронная почта</label>
                                                                <input type="email" name="email"></div>
                                                            <div class="qf3 qfselect">
                                                                <label class="qf3label">Тема обращения</label>
                                                                <select name="theme[]">
                                                                    <option value="">
                                                                        Аренда авто без водителя
                                                                    </option>
                                                                    <option value="2">Другой вопрос</option>
                                                                </select>
                                                            </div>
                                                            <div class="relatedblock" style="opacity: 1;">
                                                                <div class="qf3 qfselect">
                                                                    <label class="qf3label">Аренда авто без
                                                                        водителя</label>
                                                                    <select name="model[]">
                                                                        @foreach($list as $index => $value)
                                                                            <option
                                                                                value="{{$index}}">{{$value['name']}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="qf3 qf3txt qfdate">
                                                                    <label class="qf3label">Дата начала аренды</label>
                                                                    <input type="date" name="date_from">
                                                                </div>
                                                                <div class="qf3 qf3txt qfdate">
                                                                    <label class="qf3label">Дата возврата авто</label>
                                                                    <input type="date" name="date_till">
                                                                </div>
                                                            </div>
                                                            <div class="qf3 qftextarea">
                                                                <label class="qf3label">Комментарий</label>
                                                                <textarea name="comment"></textarea>
                                                            </div>
                                                            Внимание! Нажимая кнопку "отправить" Вы автоматически даете
                                                            свое
                                                            согласие
                                                            на хранение и обработку Ваших персональных данных
                                                            <div class="qf3 qfsubmit">
                                                                <label class="qf3label"></label>
                                                                <button> Отправить</button>
                                                            </div>
                                                            <div class="qfcapt nfl">
                                                                <a href="http://plasma-web.ru"
                                                                   target="_blank">QuickForm</a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <p></p>
                                                    <p>Телефон: {{config('app.phone_number')}}</p>
                                                    <p>
                                                        {{mb_strtoupper(config('app.name'))}}
                                                        <br>
                                                        {{config('app.address')}}
                                                        <br>
                                                        директор {{(config('app.director'))}}
                                                        <br><br>
                                                    </p>
                                                    <p><!--{/content}--></p></div>
                                            </div><!--[/html]--><p></p></div>
                                    </article>
                                    <div style="height:0.9375px" class="bd-empty-grid-item"></div>
                                    <div class="bd-container-inner" style="position: relative;">
                                        <div class="bd-containereffect-3 container-effect container "></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
