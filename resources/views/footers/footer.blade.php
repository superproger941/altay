<footer class=" bd-footerarea-1 bd-margins">
    <section class=" bd-section-2 bd-tagstyles" id="section4" data-section-title="Footer Four Columns Dark">
        <div class="bd-container-inner bd-margins clearfix" style="flex-direction: column;">
            <div class=" bd-layoutcontainer-11 bd-columns bd-no-margins">
                <div class="bd-container-inner">
                    <div class="container-fluid">
                        <div class="row bd-row-flex bd-row-align-top">
                            <div class=" bd-columnwrapper-18 col-md-4 col-sm-6">
                                <div class="bd-layoutcolumn-18 bd-column">
                                    <div class="bd-vertical-align-wrapper">
                                        <div class=" bd-joomlaposition-2 clearfix">
                                            <div class=" bd-block bd-own-margins ">
                                                <div class="bd-blockcontent bd-tagstyles">
                                                    <div class="custom">
                                                        <h2>О НАС</h2>
                                                        <p style="text-align: justify;">Компания "{{config('app.name')}}"
                                                            предлагает
                                                            аренду автомобилей в Республике Алтай для тех, кто хочет
                                                            самостоятельно путешествовать и открывать для себя
                                                            красоту
                                                            Алтая</p></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class=" bd-columnwrapper-9 col-md-4 col-sm-6">
                                <div class="bd-layoutcolumn-9 bd-column">
                                    <div class="bd-vertical-align-wrapper">
                                        <div class=" bd-joomlaposition-4 clearfix">
                                            <div class=" bd-block bd-own-margins ">

                                                <div class="bd-blockcontent bd-tagstyles">


                                                    <div class="custom">
                                                        <h2>КОНТАКТЫ</h2>
                                                        <p style="text-align: justify;">
                                                            Телефон:&nbsp;{{config('app.phone_number')}}</p>
                                                        <p>Электронная почта:&nbsp;<span class="user-account__name"><a
                                                                    href="mailto:autoprokat04@gmail.com">autoprokat04@gmail.com</a></span>
                                                        </p>
                                                        <p>Адрес: {{config('app.address')}}</p></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class=" bd-columnwrapper-27 col-md-4 col-sm-6">
                                <div class="bd-layoutcolumn-27 bd-column">
                                    <div class="bd-vertical-align-wrapper">
                                        <div class=" bd-joomlaposition-6 clearfix">
                                            <div class=" bd-block bd-own-margins ">
                                                <div class="bd-blockcontent bd-tagstyles">
                                                    <div class="custom">
                                                        <h2>МЫ В СОЦСЕТЯХ</h2>
                                                        <p style="text-align: justify;"><a
                                                                href="https://www.instagram.com/altai_avto_/"
                                                                target="_blank" rel="noopener"><img
                                                                    src="{{config('app.url')}}/img/instagram1.png"
                                                                    alt="instagram1"
                                                                    width="30" height="30"></a>&nbsp;<img
                                                                src="{{config('app.url')}}/img/Telegram.png"
                                                                alt="Telegram" width="30"
                                                                height="30"></p></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class=" bd-joomlaposition-10 clearfix">
                <div class=" bd-block bd-own-margins ">
                    <div class="bd-blockcontent bd-tagstyles">
                        <div class="custom">
                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="1"
                                   class="text_item_b">
                                <tbody>
                                <tr>
                                    <td align="center" valign="top" class="mainmenu_item">
                                        <p>Copyright © 2021 {{mb_strtoupper(config('app.name'))}} - Прокат автомобилей в Республике Алтай</p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>
