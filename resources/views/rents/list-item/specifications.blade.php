<p><strong>Тех. характеристики:</strong></p>
<table style="width: 40%;">
    <tbody>
    <tr>
        <td>
            <p>
                <span style="font-size: 8pt;"><strong>Год выпуска</strong></span>
            </p>
        </td>
        <td>
            <p>
                <span style="font-size: 8pt;">{{$item['specifications']['year']}}</span>
            </p>
        </td>
        <td style="width: 10px;">&nbsp;</td>
        <td><span style="font-size: 8pt;"><strong>Кузов</strong></span>
        </td>
        <td><span style="font-size: 8pt;">{{$item['specifications']['carcase']}}</span>
        </td>
    </tr>
    <tr>
        <td>
            <p>
                <span style="font-size: 8pt;"><strong>Объем двигателя</strong></span>
            </p>
        </td>
        <td>
            <p>
                <span style="font-size: 8pt;">{{$item['specifications']['engine_volume']}} см<sup>3</sup></span>
            </p>
        </td>
        <td>&nbsp;</td>
        <td>
            <span style="font-size: 8pt;"><strong>Привод</strong></span>
        </td>
        <td>
            <span style="font-size: 8pt;">{{$item['specifications']['drive_unit']}}</span>
        </td>
    </tr>
    <tr>
        <td>
            <p>
                <span style="font-size: 8pt;"><strong>Коробка передач</strong></span>
            </p>
        </td>
        <td>
            <p>
                <span style="font-size: 8pt;">{{$item['specifications']['transmission']}}</span>
            </p>
        </td>
        <td>&nbsp;</td>
        <td>
            <span style="font-size: 8pt;"><strong>Потребление топлива</strong></span>
        </td>
        <td>
            <span style="font-size: 8pt;">{{$item['specifications']['fuel_consumption']}}</span>
        </td>
    </tr>
    <tr>
        <td>
            <p>
                <span style="font-size: 8pt;"><strong>Мощность двигателя</strong></span>
            </p>
        </td>
        <td>
            <p>
                <span style="font-size: 8pt;">{{$item['specifications']['engine_power']}}</span>
            </p>
        </td>
        <td>&nbsp;</td>
        <td>
            <span style="font-size: 8pt;"><strong>Цвет</strong></span>
        </td>
        <td>
            <span style="font-size: 8pt;">{{$item['specifications']['color']}}</span>
        </td>
    </tr>
    <tr>
        <td>
            <p>
                <strong><span style="font-size: 8pt;">Тип топлива</span></strong>
            </p>
        </td>
        <td>
            <p>
                <span style="font-size: 8pt;">{{$item['specifications']['fuel_type']}}</span>
            </p>
        </td>
        <td>&nbsp;</td>
        <td>
            <span style="color: #339966; font-size: 8pt;"><strong>Стоимость</strong></span>
        </td>
        <td>
            <span style="font-size: 8pt;">&nbsp;
                <strong>
                    <span style="color: #339966;">{{$item['specifications']['cost']}}</span>
                </strong>
            </span>
        </td>
    </tr>
    </tbody>
</table>
