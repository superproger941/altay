@extends('layouts.default')

@section('title', config('app.name') . ' - Прокат автомобилей в Горном Алтае')

@section('content')
    <div class="bd-contentlayout-5 bd-sheetstyles-4  bd-no-margins bd-margins">
        <div class="bd-container-inner">
            <div class="bd-flex-vertical bd-stretch-inner bd-contentlayout-offset">
                <div class="bd-flex-horizontal bd-flex-wide bd-no-margins">
                    <div class="bd-flex-vertical bd-flex-wide bd-no-margins">
                        <div class=" bd-layoutitemsbox-22 bd-flex-wide bd-no-margins">
                            <div class=" bd-content-4">
                                <div class=" bd-blog " itemscope="" itemtype="http://schema.org/Blog">
                                    <div class="bd-container-inner">
                                        <div class=" bd-grid-5 bd-margins">
                                            <div class="container-fluid">
                                                <div class="separated-grid row">
                                                    @foreach ($list as $item)
                                                        @include('rents.list-item', ['item' => $item])
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
