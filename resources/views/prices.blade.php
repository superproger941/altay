@extends('layouts.default')

@section('title', 'Цены | ' . config('app.site_title') . ' | '. config('app.name'))

@section('description', 'Цены на прокат/аренду авто в Республике Алтай. '. config('app.name'))

@section('content')
    <div class=" bd-content-7">
        <div class=" bd-blog-5 bd-page-width  " itemscope="" itemtype="http://schema.org/Article">
            <div class="bd-container-inner">
                <div class=" bd-grid-7 bd-margins">
                    <div class="container-fluid">
                        <div class="separated-grid row">
                            <div class="separated-item-46 col-md-12 first-col last-row last-col"
                                 style="overflow: visible; height: auto;">

                                <div class="bd-griditem-46">

                                    <article class=" bd-article-4" style="position: relative;">
                                        <div class=" bd-pagetitle-3 bd-background-width ">
                                            <div class="bd-container-inner">
                                                <h1>
                                                    Цены - Стоимость проката автомобилей без водителя </h1>
                                            </div>
                                        </div>

                                        <div class=" bd-postcontent-4 bd-tagstyles bd-contentlayout-offset"
                                             itemprop="articleBody">
                                            <p><!--[html]-->
                                                <style></style>
                                            </p>
                                            <div class="bd-tagstyles  additional-class-840250890 ">
                                                <div class="bd-container-inner bd-content-element"><!--{content}-->
                                                    <p></p>
                                                    <h3>Стоимость аренды автомобиля без водителя</h3>
                                                    <p><strong>Суточный пробег автомобиля не ограничен</strong></p>
                                                    <table style="width: 100%;">
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <p><strong>Марка авто</strong></p>
                                                            </td>
                                                            <td>
                                                                <p><strong>1-2 дня</strong></p>
                                                            </td>
                                                            <td>
                                                                <p><strong>3-7 дней</strong></p>
                                                            </td>
                                                            <td>
                                                                <p><strong>8-20 дней</strong></p>
                                                            </td>
                                                            <td>
                                                                <p><strong>21+ суток</strong></p>
                                                            </td>
                                                        </tr>
                                                        @foreach($list as $index => $value)
                                                            <tr>
                                                                <td>
                                                                    <p>{{$value['name']}}</p>
                                                                </td>
                                                                <td>
                                                                    <p>{{(int) filter_var($value['specifications']['cost'], FILTER_SANITIZE_NUMBER_INT)}}</p>
                                                                </td>
                                                                <td>
                                                                    <p>{{(int) filter_var($value['specifications']['cost'], FILTER_SANITIZE_NUMBER_INT) - 200}}</p>
                                                                </td>
                                                                <td>
                                                                    <p>{{(int) filter_var($value['specifications']['cost'], FILTER_SANITIZE_NUMBER_INT) - 200*2}}</p>
                                                                </td>
                                                                <td>
                                                                    <p>{{(int) filter_var($value['specifications']['cost'], FILTER_SANITIZE_NUMBER_INT) - 200*3}}</p>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                    <p>&nbsp;</p>
                                                    <h3>Дополнительные услуги</h3>
                                                    <p>• Аренда детского кресла - бесплатно<br>• Аренда антирадара -
                                                        бесплатно<br>•&nbsp;Запись в договоре аренды дополнительного
                                                        водителя — бесплатно<br>• Аренда GPS-навигатора - 100
                                                        рублей/сутки.<br>• Подача/Возврат автомобиля в аэропорт - 500
                                                        рублей <br>• Подача/Возврат автомобиля в черте города - 500
                                                        рублей <br>• Возмещение услуг мойки ТС - 1000 р.<br>• Дозаправка
                                                        до полного бака, если автомобиль был возвращен с неполным баком
                                                        —50 рублей/литр<br>• Включенный в тариф средний суточный пробег:
                                                        250 км (т.е. за 7 суток 1750 км), Оплата сверх включенного
                                                        пробега, кроссоверы 7-15 руб/км, <br>• Опоздание на 1-2 часа -
                                                        10% от стоимости суток, но не менее 500 руб. Опоздание более 2
                                                        часов - оплата дополнительных суток</p>
                                                    <p>Более подробную информацию вы можете посмотреть перейдя на
                                                        карточку конкретного автомобиля или обратившись к менеджерам
                                                        компании.</p>
                                                    <p><!--{/content}--></p></div>
                                            </div><!--[/html]--><p></p></div>
                                    </article>
                                    <div class="bd-container-inner" style="position: relative;">
                                        <div class="bd-containereffect-3 container-effect container "></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
