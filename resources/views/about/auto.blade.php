<div>
    <a style="{{$item['styles_href']}}" href="{{route('rent-view', ['slug' => $item['slug']])}}">
        <img style="{{$item['styles_image']}}"
            src="{{config('app.url') . $item['main_img_url']}}"
            alt="5d692a68424805b12d09e055d10dfcbe"
            width="100%"
        />
    </a>
</div>
