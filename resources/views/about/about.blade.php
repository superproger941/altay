@extends('layouts.default')

@section('title', 'О нас - ' . config('app.name') . ' - ' . config('app.site_title'))

@section('description', 'О нашей компании. '. config('app.name'))

@section('content')
    <div class=" bd-content-9">
        <div class=" bd-blog-2 bd-page-width  " itemscope="" itemtype="http://schema.org/Article">
            <div class="bd-container-inner">
                <div class=" bd-grid-1 bd-margins">
                    <div class="container-fluid">
                        <div class="separated-grid row">
                            <div class="separated-item-7 col-md-12 first-col last-row last-col"
                                 style="overflow: visible; height: auto;">
                                <div class="bd-griditem-7">
                                    <article class=" bd-article-7" style="position: relative;">
                                        <div class=" bd-pagetitle-2">
                                            <div class="bd-container-inner">
                                                <h1>
                                                    О нас - {{config('app.name')}} - аренда автомобилей в Горно-Алтайске
                                                </h1>
                                            </div>
                                        </div>

                                        <div
                                            class=" bd-postcontent-7 bd-tagstyles bd-custom-blockquotes bd-custom-bulletlist bd-custom-orderedlist bd-custom-table bd-contentlayout-offset"
                                            itemprop="articleBody">
                                            <div class="bd-tagstyles  additional-class-558625897 ">
                                                <div class="bd-container-inner bd-content-element"><!--{content}-->
                                                    <p></p>
                                                    <h3>О нас</h3>
                                                    <table style="width: 100%;">
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <p>
                                                                    Компания
                                                                    "{{config('app.name')}}" предлагает аренду
                                                                    автомобилей в Республике
                                                                    Алтай
                                                                    для тех, кто хочет самостоятельно путешествовать и
                                                                    открывать для себя красоту Алтая.</p>
                                                                <p>Основной принцип работы {{config('app.name')}} -
                                                                    оказание
                                                                    качественных услуг каждому заказчику, не важно -
                                                                    обращаются к нам один раз или постоянно. Ваши
                                                                    удобство,
                                                                    комфорт, спокойствие и безопасность, а также
                                                                    поддержание
                                                                    нашей репутации - вот для чего мы трудимся каждый
                                                                    день.</p>
                                                                <p>{{config('app.name')}} - это быстро, просто, надежно!
                                                                    Мы всегда
                                                                    на
                                                                    связи: готовы оперативно решать ваши транспортные
                                                                    задачи
                                                                    и отвечать на возникающие вопросы по телефону:
                                                                    {{config('app.phone_number')}}</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <h3>Прокат авто</h3>
                                                    <p>Есть сотни обстоятельств, когда ни один вид передвежения не
                                                        заменит
                                                        поездку на собственном автомобиле. На случай, если Ваш
                                                        автомобиль по
                                                        каким-то причинам не может Вам в этом помочь, то мы с радостью
                                                        предложим аренду любого из шести наших.&nbsp;
                                                    </p>
                                                    <p>
                                                        Посмотреть все фотографии и ознакомиться с характеристиками
                                                        машин Вы можете в разделе <a style="color: blue;"
                                                                                     href="{{route('rent.index', [])}}">"Аренда
                                                            авто"</a>
                                                    </p>
                                                    <div style="display: flex; width: 100%;">
                                                        @foreach ($list as $item)
                                                            @include('about.auto', ['item' => $item])
                                                        @endforeach
                                                    </div>
                                                    <h3><br>Трансфер</h3>
                                                    <p>Прокат автомобилей с личным водителем. Автомобиль будет доставлен&nbsp;нашим
                                                        водителем в аэропорт или на любой другой адрес в пределах
                                                        Республики
                                                        Алтай, Новосибирской и Кемеровской области.</p>
                                                    <p>К Вашим услугам два внедорожника премиум класса&nbsp;Toyota Land
                                                        Cruiser Prado и Toyota Land Cruiser 200. Подробнее в разделе <a
                                                            href="/transfer">Трансфер</a></p>
                                                    <h3>&nbsp;</h3>
                                                    <h3>Условия проката</h3>
                                                    <p>Внимание! Перед бронированием авто обязательно ознакомтесь с <a
                                                            href="/usloviya-prokata">условиями проката</a></p>
                                                    <h3>&nbsp;</h3>
                                                    <h3>Цены</h3>
                                                    <p>Узнать стоимость бронирования авто Вы можете перейдя в раздел <a
                                                            href="/tseny">Цены</a></p>
                                                    <h3>&nbsp;</h3>
                                                    <h3>Форма обратной связи и бронирования авто</h3>
                                                    <p></p>
                                                    <div class="qf3form simple">
                                                        <form action="/" method="post"
                                                              enctype="multipart/form-data" autocomplete="off">
                                                            {{ csrf_field() }}
                                                            <div class="qf3 qf3txt qftext req">
                                                                <label class="qf3label">
                                                                    Введите имя
                                                                    <span class="qf3labelreq">*</span>
                                                                </label>
                                                                <input type="text" name="name" required=""></div>
                                                            <div class="qf3 qf3txt qftel req">
                                                                <label class="qf3label">
                                                                    Номер телефона <span class="qf3labelreq">*</span>
                                                                </label>
                                                                <input type="tel" name="phone" required="">
                                                            </div>
                                                            <div class="qf3 qf3txt qfemail">
                                                                <label class="qf3label">Электронная почта</label>
                                                                <input type="email" name="email"></div>
                                                            <div class="qf3 qfselect">
                                                                <label class="qf3label">Тема обращения</label>
                                                                <select name="theme[]">
                                                                    <option value="">
                                                                        Аренда авто без водителя
                                                                    </option>
                                                                    <option value="2">Другой вопрос</option>
                                                                </select>
                                                            </div>
                                                            <div class="relatedblock" style="opacity: 1;">
                                                                <div class="qf3 qfselect">
                                                                    <label class="qf3label">Аренда авто без
                                                                        водителя</label>
                                                                    <select name="model[]">
                                                                        @foreach($list as $index => $value)
                                                                            <option
                                                                                value="{{$index}}">{{$value['name']}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="qf3 qf3txt qfdate">
                                                                    <label class="qf3label">Дата начала аренды</label>
                                                                    <input type="date" name="date_from">
                                                                </div>
                                                                <div class="qf3 qf3txt qfdate">
                                                                    <label class="qf3label">Дата возврата авто</label>
                                                                    <input type="date" name="date_till">
                                                                </div>
                                                            </div>
                                                            <div class="qf3 qftextarea">
                                                                <label class="qf3label">Комментарий</label>
                                                                <textarea name="comment"></textarea>
                                                            </div>
                                                            Внимание! Нажимая кнопку "отправить" Вы автоматически даете
                                                            свое
                                                            согласие
                                                            на хранение и обработку Ваших персональных данных
                                                            <div class="qf3 qfsubmit">
                                                                <label class="qf3label"></label>
                                                                <button> Отправить</button>
                                                            </div>
                                                            <div class="qfcapt nfl">
                                                                <a href="http://plasma-web.ru"
                                                                   target="_blank">QuickForm</a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <br><!--{/content}--></div>
                                            </div><!--[/html]--><p></p></div>
                                    </article>
                                    <div style="height:0.718597412109375px" class="bd-empty-grid-item"></div>
                                    <div class="bd-container-inner" style="position: relative;">
                                        <div class="bd-containereffect-1 container-effect container "></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
