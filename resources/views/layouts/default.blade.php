<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="keywords"
          content="Алтай-Авто, Алтай Авто, прокат, машин, автомобиля, Горно-Алтайск, {{config('app.name')}}, аренда, машины, авто, автомобили, Майма, kia-rio, renault logan, Республика Алтай, сколько стоит аренда авто Горно-Алтайске автомобиль на прокат взять, аренда авто, в Горно-Алтайске, прокат автомобилей цены">
    <meta name="description" content="{{config('app.name')}} - {{config('app.site_title')}}">
    <link rel="canonical" href="{{config('app.url')}}"/>
    <title>@yield('title')</title>

    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link class="" href="//fonts.googleapis.com/css?family=Montserrat:regular,700&amp;subset=latin" rel="stylesheet"
          type="text/css">

    <script async="" src="https://mc.yandex.ru/metrika/tag.js"></script>

    <link rel="stylesheet" href="{{config('app.url')}}/css/simple.css" type="text/css"/>
    <link rel="stylesheet" href="{{config('app.url')}}/css/bootstrap.min.css?version=1.0.442" type="text/css"/>
    <link rel="stylesheet" href="{{config('app.url')}}/css/template.min.css?version=1.0.442" type="text/css"/>
    <link rel="stylesheet" href="{{config('app.url')}}/css/main.css" type="text/css"/>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (m, e, t, r, i, k, a) {
            m[i] = m[i] || function () {
                (m[i].a = m[i].a || []).push(arguments)
            };
            m[i].l = 1 * new Date();
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
        })
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(80094520, "init", {
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true
        });
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/80094520" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
</head>

<body class="bootstrap bd-body-1 bd-homepage bd-pagebackground-17 bd-margins">
@include('headers.header')
@yield('content')
@include('footers.footer')
<div data-smooth-scroll="" data-animation-time="250" class="bd-smoothscroll-3">
    <a href="#" class="bd-backtotop-1 " style="display: none;">
        <span class="bd-icon-66 bd-icon "></span>
    </a>
</div>
</body>
</html>
